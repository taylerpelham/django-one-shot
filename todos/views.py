from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm

# Create your views here.


def update_todo_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid:
            form = form.save()
            return redirect("todo_list_detail", form.list.id)
    else:
        form = TodoItemForm(instance=item)
    context = {"form": form, "item": item}
    return render(request, "todos/update_todo_item.html", context)


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid:
            form = form.save()
            return redirect("todo_list_detail", form.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/create_todo_item.html", context)


def delete_todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete_todo.html")


def update_todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid:
            form = form.save()
            return redirect("todo_list_detail", form.id)
    else:
        form = TodoListForm(instance=todo)
    context = {"form": form, "todo": todo}
    return render(request, "todos/update_todo.html", context)


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {"todos": todos}
    return render(request, "todos/todos_list.html", context)


def todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {"todo": todo}
    return render(request, "todos/todo_detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid:
            form = form.save()
            return redirect("todo_list_detail", form.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create_todo.html", context)
