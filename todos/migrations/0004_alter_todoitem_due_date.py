# Generated by Django 5.0 on 2023-12-12 18:45

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0003_rename_todolistitem_todoitem"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todoitem",
            name="due_date",
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
