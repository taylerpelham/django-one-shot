from django.urls import path
from .views import (
    todo_list_list,
    todo,
    create_todo,
    update_todo,
    delete_todo,
    create_todo_item,
    update_todo_item,
)

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo, name="todo_list_detail"),
    path("create/", create_todo, name="todo_list_create"),
    path("<int:id>/edit/", update_todo, name="todo_list_update"),
    path("<int:id>/delete/", delete_todo, name="todo_list_delete"),
    path("items/create/", create_todo_item, name="todo_item_create"),
    path("items/<int:id>/edit/", update_todo_item, name="todo_item_update"),
]
